
#include <stdio.h>
#include <set>
#include <limits.h>
#include <algorithm>

/*
 * mozna zamienic set na unordered_set, ale wtedy trzeba bedzie
 * przekompilować to wszystko pod C++11, ale szkopul powinien dac temu
 * rade. Mimo wszystko, wierze, ze ten algorytm da rade.
 */

using namespace std;

int lbBaj;
int rekord = 0;

void ustawRekord(int ile) {
	if ((ile > rekord) && (ile <= lbBaj)) {
		rekord = ile;
		printf("nowy rekord %i\n", rekord);
	}
}

int main(){
	int lbKup;
	scanf("%i%i", &lbKup, &lbBaj);
	int ileLudzi[lbKup + 2];
	int budzety[lbKup + 2];
	ileLudzi[0] = 0;
	budzety[0] = 0;
	int gdzieWrzucamy = 0;
	for (int i = 0; i < lbKup; i++) {
		int budzet;
		scanf("%i", &budzet);
		if (budzet == budzety[gdzieWrzucamy]) {
			ileLudzi[gdzieWrzucamy]++;
		} else {
			ileLudzi[++gdzieWrzucamy] = 1;
			budzety[gdzieWrzucamy] = budzet;
		}
	}
	ileLudzi[++gdzieWrzucamy] = 0;
	budzety[gdzieWrzucamy] = INT_MAX;
	set<int> ileMoznaWyplacic[lbKup + 2];
	for (int i = 0; i <= gdzieWrzucamy; i++) {
		ileMoznaWyplacic[i].insert(0);
	}

	//trzeba pojechac od konca i zebrac sumy tak, zeby bylo, ile kto ma kasy
	//tzn ze ileLudzi[i] pokazuje, ile ludzi ma przynajmniej tyle pieniedzy
	//ile jest w budzety[i]
	for (int i = gdzieWrzucamy -1; i > 0; i--) {
		ileLudzi[i] += ileLudzi[i + 1];
		//ileLudzi[i] = ileLudzi[gdzieWrzucamy - i];
		//ileLudzi[gdzieWrzucamy - i] = a;
	}
	/*
	 * W ileLudzi[i] jest liczba ludzi, ktorzy dysponuja kwota przynajmniej
	 * budzety[i].
	 * W budzety[i] jest tez kwota 0 ustawiona na sztywno i MAX_INT, zeby
	 * miec straznikow.
	 * budzety[gdzieWrzucamy] == MAX_INT.
	 * budzety[i] jest uporzadkowana rosnaco.
	 * ileLudzi jest uporzadkowana malejaco.
	 */

	/*
	 * Teraz nalezy tylko przejsc od i = gdzieWrzucamy - 1 do 1.
	 * Wtedy musimy przeiterowac po kazdej liczbie monet m, jaka mozemy
	 * sprzedac ludziom, ktorych majatek wynosi dokladnie budzety[i].
	 * Robimy to tak, aby nic nie sprzedawac ludziom, ktorych majatek
	 * wynosi budzety[i - 1].
	 * Dla kazdego m liczymy ileLudzi[
	 */

	for (int i = gdzieWrzucamy - 1; i > 0; i--) {
		for (int j = budzety[i]; j > budzety[i - 1]; j--) {
			int ileDodac = (ileLudzi[i] - ileLudzi[i + 1]) * j;
			for (set<int>::iterator first =
						ileMoznaWyplacic[i + 1].begin();
			first != ileMoznaWyplacic[i + 1].end(); first++) {
				if ((*first) == 0) {
					ileMoznaWyplacic[i].insert
							(ileLudzi[i] * j);
					ustawRekord(ileLudzi[i] * j);
				} else {
					ileMoznaWyplacic[i].insert
							((*first) + ileDodac);
					ustawRekord((*first) + ileDodac);
				}
			}
		}
	}
	printf("%i\n", rekord);
	return 0;
}
