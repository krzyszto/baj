#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <algorithm>

int main() {
	srand(time(NULL));
	int ileLudzi = rand() % 20;
	int ileDoSprzedania = rand() % 1000;
	printf("%i %i\n", ileLudzi, ileDoSprzedania);
	unsigned int tablica[ileLudzi];
	for (int i = 0; i < ileLudzi; i++) {
		tablica[i] = abs(rand() % 2000);
	}
	std::sort(tablica, tablica + ileLudzi);
	for (int i = 0; i < ileLudzi; i++) {
		printf("%u ", tablica[i]);
	}
	printf("\n");
}
