all: baj gentest bajpala

baj: baj.cpp
	g++ -O0 -Wall -g baj.cpp -o baj
gentest: gentest.cpp
	g++ -Wall -o gentest gentest.cpp
bajpala: bajpala.cpp
	g++ -Wall -g -o bajpala bajpala.cpp
clean:
	rm baj bajpala gentest
