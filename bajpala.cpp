#include <stdio.h>
#include <vector>

using namespace std;

const int MAX_KUPUJACYCH = 1000;
const int MAX_SUMA_BUDZETOW = 10000;

int ileKasyMaSprzedawca;
int lbKupujacych;
int rekord = 0;

vector<int> ileKtoMaKasy;

bool czyJuzLiczylismy[MAX_KUPUJACYCH][MAX_SUMA_BUDZETOW];

void policz(int ileUzbieralismy, int ileDalismyHajsu, int iluDalismy) {
	if (czyJuzLiczylismy[iluDalismy][ileDalismyHajsu]) {
		return;
	} else {
		czyJuzLiczylismy[iluDalismy][ileDalismyHajsu] = true;
	}
	for (int i = ileDalismyHajsu; i >= 0; i--) {
		if ((ileUzbieralismy + i * (ileKtoMaKasy[i] - ileKtoMaKasy[ileDalismyHajsu]) > rekord)
			&& (ileUzbieralismy + i * (ileKtoMaKasy[i] - ileKtoMaKasy[ileDalismyHajsu]) <= ileKasyMaSprzedawca)) {
				rekord = ileUzbieralismy + i * (ileKtoMaKasy[i] - ileKtoMaKasy[ileDalismyHajsu]);
			}
		policz(ileUzbieralismy + i * (ileKtoMaKasy[i] - ileKtoMaKasy[ileDalismyHajsu]), i, ileKtoMaKasy[i]);
	}
}

int main() {
	scanf("%i%i", &lbKupujacych, &ileKasyMaSprzedawca);
	for (int i = 0; i <= MAX_KUPUJACYCH; i++) {
		for (int j = 0; j <= ileKasyMaSprzedawca; j++) {
			czyJuzLiczylismy[i][j] = false;
		}
	}
	int abc[lbKupujacych];
	for (int i = 0; i < lbKupujacych; i++) {
		scanf("%i", &abc[i]);
	}
	const int nabogatszy = abc[lbKupujacych - 1];
	//int ileKtoMaKasy[nabogatszy + 1];
	for (int i = 0; i <= abc[lbKupujacych - 1]; i++) {
		ileKtoMaKasy.push_back(0);
	}
	for (int i = 0; i < lbKupujacych; i++) {
		//printf("%i\n", abc[i]);
		ileKtoMaKasy[abc[i]]++;
	}
	for (int i = nabogatszy - 1; i >= 0; i--) {
		ileKtoMaKasy[i] += ileKtoMaKasy[i + 1];
	}
	ileKtoMaKasy.push_back(0);
	policz(0, ileKtoMaKasy.size() - 1, 0);
	//void policz(int ileUzbieralismy, int ileDalismyHajsu, int iluDalismy) {
	printf("%i\n", rekord);
	return 0;
}

