#include <stdio.h>
#include <vector>
#include <limits.h>
#include <stack>
#include <assert.h>

using namespace std;

const int MAX_KUPUJACYCH = 1000;
const int MAX_SUMA_BUDZETOW = 10000;

struct trojka {
	int ileHajsu;
	int ileLudzi;
	int sumaWszystkichMniejszychWlacznie;
};

bool wypisuj = false;

bool czyJuzLiczylismy[MAX_KUPUJACYCH][MAX_SUMA_BUDZETOW];
int ileBajtow;
int rekord = 0;
vector<trojka> budzety;

void znajdzIndeks(int &ileKasy, int &indeks) {
	if (ileKasy == 0) {
		indeks = 0;
		return;
	}
	if ((ileKasy <= budzety[indeks].ileHajsu) &&
		(ileKasy > budzety[indeks - 1].ileHajsu)) {
			return;
	}
	if (ileKasy > budzety[indeks].ileHajsu) {
		indeks++;
		znajdzIndeks(ileKasy, indeks);
	} else {
		indeks--;
		znajdzIndeks(ileKasy, indeks);
	}
}

void trzaskajGraf(int naCzymSkonczylismy, int gdzieTeraz, int ileUzbieralismy, int ileOstatnioDalismy) {
	if ((ileUzbieralismy > rekord) && ileUzbieralismy <= ileBajtow) {
		rekord = ileUzbieralismy;
	}
	if (czyJuzLiczylismy[ileOstatnioDalismy][ileUzbieralismy]) {
		return;
	} else {
		czyJuzLiczylismy[ileOstatnioDalismy][ileUzbieralismy] = true;
	}
	int gdzieKoniec = gdzieTeraz;
	for (int i = budzety[gdzieTeraz].ileHajsu; i > 0 ; i--) {
		znajdzIndeks(i, gdzieKoniec);
		assert((budzety[gdzieKoniec].ileHajsu >= i) && (budzety[gdzieKoniec - 1].ileHajsu < i));
		int tmpUzbierane = ileUzbieralismy + i * (budzety[gdzieKoniec].ileLudzi - budzety[naCzymSkonczylismy].ileLudzi);
		//if (tmpUzbierane + budzety[gdzieKoniec - 1].sumaWszystkichMniejszychWlacznie < rekord) {
		//	return;
		//}
		if ((tmpUzbierane > rekord) && (tmpUzbierane <= ileBajtow)) {
			rekord = tmpUzbierane;
		}
		if (tmpUzbierane < ileBajtow) {
			trzaskajGraf(gdzieTeraz, gdzieKoniec - 1, tmpUzbierane, i);
		}
	}
}


int main() {
	int ileKupujacych;
	scanf("%i%i", &ileKupujacych, &ileBajtow);
	trojka tmp6;
	tmp6.ileHajsu = 0;
	tmp6.ileLudzi = 0;
	budzety.push_back(tmp6);
	for (int i = 0; i < ileKupujacych; i++) {
		int a;
		scanf("%i", &a);
		if (a == budzety.back().ileHajsu) {
			trojka tmp = budzety.back();
			tmp.ileLudzi++;
			budzety.pop_back();
			budzety.push_back(tmp);
		} else {
			trojka tmp;
			tmp.ileHajsu = a;
			tmp.ileLudzi = 1;
			budzety.push_back(tmp);
		}
	}
	tmp6.ileHajsu = INT_MAX;
	budzety.push_back(tmp6);
	for (int i = budzety.size(); i > 0; i--) {
		budzety[i - 1].ileLudzi += budzety[i].ileLudzi;
	}
	budzety[0].sumaWszystkichMniejszychWlacznie = 0;
	for (unsigned int i = 1; i < budzety.size(); i++) {
		budzety[i].sumaWszystkichMniejszychWlacznie = budzety[i - 1].sumaWszystkichMniejszychWlacznie + budzety[i].ileHajsu * (budzety[i].ileLudzi - budzety[i + 1].ileLudzi);
	}
	budzety[budzety.size() - 1].sumaWszystkichMniejszychWlacznie = budzety[budzety.size() - 2].sumaWszystkichMniejszychWlacznie;
	/*
	 * Mamy teraz wektor, ktory sie sklada z par
	 * <ileHajsu, ileLudziMaTenHajs, sumaWszystkichMniejszychWlacznie>
	 */
	for (int i = 0; i < ileKupujacych; i++) {
		for (int j = 0; j <= ileBajtow; j++) {
			czyJuzLiczylismy[i][j] = false;
		}
	}
	if (budzety.size() > 4) {
		trzaskajGraf(budzety.size() - 1, budzety.size() - 3, 0, 0);
	} else {
		trzaskajGraf(budzety.size() - 1, budzety.size() - 2, 0, 0);
	}
	printf("%i\n", rekord);
	return 0;
}
