#include <stdio.h>
#include <set>
#include <limits.h>
#include <algorithm>
#include <assert.h>
#include <stack>

/*
 * mozna zamienic set na unordered_set, ale wtedy trzeba bedzie
 * przekompilować to wszystko pod C++11, ale szkopul powinien dac temu
 * rade. Mimo wszystko, wierze, ze ten algorytm da rade.
 */

using namespace std;

int lbBaj;
int rekord = 0;

struct para {
	int indeksTablicyBudzetow;
	int ileWydalismyHajsu;
	int ileOstatnioDalismy;
};

/*void ustawRekord(para mojaPara) {
	if ((mojaPara.ileJuzSprzedalismy > rekord) &&
	(mojaPara.ileJuzSprzedalismy <= lbBaj)) {
		rekord = mojaPara.ileJuzSprzedalismy;
	}
}*/

typedef stack<para> stospar;

stospar stos;	/*first opisuje, ktory z tablicy ludzi
		 * dostal wyplate,
		 * second opisuje, ile dostal.
		 * first daje "ile ludzi o roznych majatkach..."
		 */

void ustawRekord(para &p) {
	if ((p.ileWydalismyHajsu > rekord) && (p.ileWydalismyHajsu <= lbBaj)) {
		rekord = p.ileWydalismyHajsu;
	}
}

bool rowne(para &a, para &b) {
	if (a.indeksTablicyBudzetow != b.indeksTablicyBudzetow) {
		return false;
	}
	if (a.ileWydalismyHajsu != b.ileWydalismyHajsu) {
		return false;
	}
	if (a.ileOstatnioDalismy != b.ileOstatnioDalismy) {
		return false;
	}
	return true;
}

int main() {
	int lbKup;
	scanf("%i%i", &lbKup, &lbBaj);
	if ((lbKup == 0) || (lbBaj == 0)) {
		printf("0\n");
		return 0;
	}

	//int* ileLudzi = new int[lbKup + 2];
	//int* budzety = new int[lbKup + 2];
	int ileLudzi[lbKup + 2];
	int budzety[lbKup + 2];
	ileLudzi[0] = 0;
	budzety[0] = 0;

	int gdzieWrzucamy = 0;
	for (int i = 0; i < lbKup; i++) {
		int budzet;
		scanf("%i", &budzet);
		if (budzet == budzety[gdzieWrzucamy]) {
			ileLudzi[gdzieWrzucamy]++;
		} else {
			ileLudzi[++gdzieWrzucamy] = 1;
			budzety[gdzieWrzucamy] = budzet;
		}
	}

	ileLudzi[++gdzieWrzucamy] = 0;
	budzety[gdzieWrzucamy] = INT_MAX;

	/*
	 * gdzieWrzucamy pokazuje teraz na straznika, tzn 0 liczbe osob, ktore
	 * dysponuja kwota INT_MAX;
	 */

	//trzeba pojechac od konca i zebrac sumy tak, zeby bylo, ile kto ma kasy
	//tzn ze ileLudzi[i] pokazuje, ile ludzi ma przynajmniej tyle pieniedzy
	//ile jest w budzety[i]
	for (int i = gdzieWrzucamy -1; i > 0; i--) {
		ileLudzi[i] += ileLudzi[i + 1];
	}
	for (int i = gdzieWrzucamy - 1; i > 0; i--) {
		int a = ileLudzi[gdzieWrzucamy - i];
		ileLudzi[gdzieWrzucamy - i] = ileLudzi[i];
		ileLudzi[i] = a;
	}

	//int* turboTablica = new int[budzety[gdzieWrzucamy - 1] + 1];
	int turboTablica[budzety[gdzieWrzucamy - 1] + 1];
	//zerowanie TurboTablicy
	for (int i = 0; i < budzety[gdzieWrzucamy - 1] + 1; i++) {
		turboTablica[i] = -1;
	}

	/*
	 * Super turbotablica uzupelniania.
	 */
	for (int i = gdzieWrzucamy - 1; i >= 0; i--) {
		turboTablica[budzety[i]] = ileLudzi[i];
	}

	//wkladanie sensownych danych do turbotablicy
	for (int i = budzety[gdzieWrzucamy - 1]; i >= 0; i--) {
		if (turboTablica[i] == -1) {
			turboTablica[i] = turboTablica[i+1];
		}
	}

	int sumaWszystkichBudzetow[gdzieWrzucamy + 1];
	sumaWszystkichBudzetow[0] = 0;

	for (int i = 1; i < gdzieWrzucamy; i++) {
		sumaWszystkichBudzetow[i] = sumaWszystkichBudzetow[i - 1] + budzety[i] * (ileLudzi[i] - ileLudzi[i + 1]);
	}
	sumaWszystkichBudzetow[gdzieWrzucamy] = sumaWszystkichBudzetow[gdzieWrzucamy - 1];
	sumaWszystkichBudzetow[gdzieWrzucamy + 1] = sumaWszystkichBudzetow[gdzieWrzucamy];

	/*
	 * W ileLudzi[i] jest liczba ludzi, ktorzy dysponuja kwota przynajmniej
	 * budzety[i].
	 * W budzety[i] jest tez kwota 0 ustawiona na sztywno i MAX_INT, zeby
	 * miec straznikow.
	 * budzety[gdzieWrzucamy] == MAX_INT.
	 * budzety[i] jest uporzadkowana rosnaco.	WAZNE!!!
	 * ileLudzi jest uporzadkowana malejaco.	WAZNE!!!
	 */

	/*
	 * Teraz nalezy tylko przejsc od i = gdzieWrzucamy - 1 do 1.
	 * Wtedy musimy przeiterowac po kazdej liczbie monet m, jaka mozemy
	 * sprzedac ludziom, ktorych majatek wynosi dokladnie budzety[i].
	 * Robimy to tak, aby nic nie sprzedawac ludziom, ktorych majatek
	 * wynosi budzety[i - 1].
	 */

	para czubekstosu;
	czubekstosu.ileWydalismyHajsu = 0;
	czubekstosu.indeksTablicyBudzetow = gdzieWrzucamy - 1;
	czubekstosu.ileOstatnioDalismy = 0;
	stos.push(czubekstosu);	//bedziemy placic przedostatniemu czlowiekowi (najbogatszemu), wydalismy 0
	while (!stos.empty()) {
		para element = stos.top();
		stos.pop();
		printf("CURRENT: indx=%i, ileWydane=%i, ileTeraz=%i\n", element.indeksTablicyBudzetow, element.ileWydalismyHajsu, element.ileOstatnioDalismy);
		ustawRekord(element);
		if (!(element.ileWydalismyHajsu + sumaWszystkichBudzetow[element.indeksTablicyBudzetow - 1] > rekord)) {
			continue;
		}
		int ostatnieMiejsceWBudzetach = 0;
		for (int i = 1; i <= budzety[element.indeksTablicyBudzetow] ; i++) {
			while (budzety[ostatnieMiejsceWBudzetach] < i) {
				ostatnieMiejsceWBudzetach++;
			}
			if (ostatnieMiejsceWBudzetach == element.indeksTablicyBudzetow && !rowne(element, czubekstosu)) {
				continue;
			}
			para tmp;
			tmp.indeksTablicyBudzetow = ostatnieMiejsceWBudzetach;
			tmp.ileWydalismyHajsu = element.ileWydalismyHajsu + i * (turboTablica[i] - turboTablica[element.ileOstatnioDalismy]);
			tmp.ileOstatnioDalismy = i;
			if ((tmp.ileWydalismyHajsu + sumaWszystkichBudzetow[tmp.indeksTablicyBudzetow - 1] > rekord) && (tmp.ileWydalismyHajsu <= lbBaj)) {
				stos.push(tmp);
				printf("+ indx=%i, ileWydane=%i, ileTeraz=%i\n", tmp.indeksTablicyBudzetow, tmp.ileWydalismyHajsu, tmp.ileOstatnioDalismy);
			} else {
				//printf("- indx=%i, ileWydane=%i, ileTeraz=%i\n", tmp.indeksTablicyBudzetow, tmp.ileWydalismyHajsu, tmp.ileOstatnioDalismy);
			}
		}
	}
	printf("%i\n", rekord);
	return 0;
}

/*
 * Powiedzmy, ze mamy pare <a, b> - znaczy, ze wydalismy w sumie b pieniedzy,
 * dajac je ileLudzi[a] ludziom. Kolejny czlowiek, ktoremu bedziemy
 * dawac dysponuje kwota budzety[a]. Ludzi, ktorzy dysponuja przynajmniej jego
 * kwota jest ileLudzi[a].
 * Po pierwsze, w tym momencie, dokladnie ileLudzi[a] - ileLudzi[a + 1] nie
 * dostalo pieniedzy.
 * Co nalezy zrobic:
 * dla kazdego j <= budzety[a]; j > 0 trzeba znalezc, ile ludzi jeszcze nie
 * dostalo przydzialu i ile ludziom bedziemy sprzedawac j pieniedzy.
 * Jak sprawdzic, ile ludzi ma tyle pieniedzy?
 * Po prostu, jesli j <= budzety[a - 1] to a--;
 */
